DROP DATABASE IF EXISTS gym;
CREATE DATABASE gym;
use gym;

CREATE TABLE user (
    name        TEXT          NOT NULL,
    description TEXT          NOT NULL,
    PRIMARY KEY (name)
)

CREATE TABLE personal_record (
    id          INT           NOT NULL AUTO_INCREMENT,
    user_name   TEXT           NOT NULL,
    record      INT           NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_name) REFERENCES user(name)
);