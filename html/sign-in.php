<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-up</title>
    <?php require_once __DIR__ . "/../src/live-reload.php"; ?>
    <script src="https://cdn.tailwindcss.com"></script>

</head>

<body class="bg-sky-600">
    <main class = "flex">
        <form method="post">
            <label for="email">Email:</label>
            <input type="text" name="email" id="email">

            <label for="password">Mot de passe:</label>
            <input type="text" name="password" id="password">

            <label for="password_confirmation">Confirmation du mot de passe:</label>
            <input type="text" name="password_confirmation" id="password_confirmation">

            <button type="submit">Envoyer</button>
        </form>
    </main>
</body>

</html>