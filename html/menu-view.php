<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Menu</title>
    <?php require_once __DIR__."/../src/live-reload.php"; ?>
    <link rel="stylesheet" href="../css/styles.css">
</head>
<body class="min-h-screen">
    <header class="grid grid-cols-6 border-b-4 text-center relative z-0">
        <span class="col-span-6 text-5xl pb-1">Bienvenue !</span>
    </header>

    <div class="hamburgerDiv">
        <label class="hamburger-menu absolute z-30">
            <input type="checkbox">
            <span class="close absolute left-13 bottom-2 z-100">fermer</span>
        </label>
        
        <aside class="sidebar pl-1.5 absolute top-0 left-0 w-full z-20">
            <nav class="flex flex-col pt-10 text-end pr-3">
                <a class="text-xl" href="menu.php">Accueil</a>
                <a class="text-xl" href="graphique.php">Graphique</a> 
                <a class="text-xl" href="record.php">Record Personnel</a>
                <div>etc...</div>
            </nav>
        </aside>
    </div>
</body>
</html>