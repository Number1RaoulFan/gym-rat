<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page d'Accueil</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <?php require_once __DIR__."/../src/live-reload.php"; ?>
</head>

<?php
 const BUTTON_STYLE = "transition ease-in-out delay-100 bg-sky-300 rounded-md p-2 px-4 m-6 shadow-lg hover:bg-sky-400 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-30";
?>


<body class="min-h-screen bg-sky-600">
    <header>
        <h1 class="flex text-5xl font-bold text-blue-900 p-8 justify-center">Hello gym rats!</h1>
    </header>
    <main class="grid grid-cols-2 gap-8 px-10">
        <div class="flex flex-col items-center justify-center border-blue-900 shadow-lg bg-sky-700">
            <img src="../img/lebron.jpg" alt="Le sunshine" class="rounded-full w-1/5">
            <p class="text-black p-4">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, magnam blanditiis! Pariatur
                numquam cum iste adipisci et eveniet molestiae aut sunt veniam, itaque dolor, repellendus dicta
                accusantium commodi, inventore soluta similique magnam delectus? Iure esse omnis consequuntur autem,
                laudantium tempore reprehenderit eaque eum possimus minima delectus nisi repudiandae velit dolore quod.
                Cum, odio, quas dicta pariatur ipsam nobis, labore deserunt a odit harum reiciendis. Architecto
                distinctio blanditiis explicabo animi amet sit temporibus illum repudiandae nulla, dolorum omnis
                nesciunt, perspiciatis esse!
            </p>
            <div class="flex justify-evenly w-full">
                <a href="menu.php" class="<?=BUTTON_STYLE?>">Créer un compte</a>
                <a href="sign-in.php" class="<?=BUTTON_STYLE?>">Se connecter</a>
            </div>
        </div>


        <div class="flex cols-span-2 justify-around">
            <img src="../img/trentwinsM.jpg" alt="image anabolique" class=" rounded border-4 border-blue-900 shadow-lg">
        </div>
    </main>
</body>
</body>

</html>
